# Vue.js Todo List

> A Todo List made with Vue.js

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

*** Routing ***
https://vuejs.org/v2/guide/routing.html
https://graphql.org/code/#javascript
https://graphql.org/learn/

